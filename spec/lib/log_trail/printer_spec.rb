# frozen_string_literal: true

require 'spec_helper'

require_relative './../../../lib/log_trail/printer'

describe LogTrail::Printer do
  let(:visits) do
    [
      ['/about', 2],
      ['/home', 1],
      ['/contact', 3]
    ]
  end

  let(:unique_visits) do
    [
      ['/contact', 1],
      ['/home', 1]
    ]
  end

  subject { described_class.new }

  describe '#print' do
    it 'prints the visit count' do
      expect($stdout).to receive(:puts).with('Routes visit count')
      expect($stdout).to receive(:puts).with('/about 2 visits')
      expect($stdout).to receive(:puts).with('/home 1 visit')
      expect($stdout).to receive(:puts).with('/contact 3 visits')

      expect($stdout).to receive(:puts).with("Routes unique visit count\n")

      expect($stdout).to receive(:puts).with('/contact 1 unique view')
      expect($stdout).to receive(:puts).with('/home 1 unique view')

      subject.print(visits, unique_visits)
    end
  end
end
