# frozen_string_literal: true

class LogTrail
  # Store visits and unique visits
  class Storage
    def initialize
      @visits = Hash.new { |route, ip| route[ip] = [] }
    end

    def add(route, ip)
      @visits[route] << ip
    end

    def visits_count
      sort(visits.map { |route, ip| [route, ip.count] })
    end

    def unique_visits_count
      sort(visits.map { |route, ip| [route, ip.uniq.count] })
    end

    def count_by(route)
      visits[route].count
    end

    private

    attr_reader :visits

    def sort(arr)
      arr.sort { |a, b| b[1] <=> a[1] }
    end
  end
end
