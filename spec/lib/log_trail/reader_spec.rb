# frozen_string_literal: true

require 'spec_helper'

require_relative './../../../lib/log_trail/reader'

describe LogTrail::FileReader do
  describe '#initialize' do
    context 'when file exists' do
      let(:file_path) { "#{Dir.pwd}/spec/fixtures/webserver.log" }

      it 'does not raise' do
        expect { described_class.new(file_path) }.not_to raise_error
      end
    end

    context 'when file does not exists' do
      let(:file_path) { "#{Dir.pwd}/spec/fixtures/webserver.logg" }

      it 'raises an error about the existence of the file' do
        expect { described_class.new(file_path) }.to raise_error FileNotProvidedError
      end
    end
  end

  describe '#each' do
    subject { described_class.new('spec/fixtures/webserver.less.log') }

    it 'yield route and ip per line' do
      routes_and_ips = []

      subject.each do |route, ip|
        routes_and_ips << "#{route} | #{ip}"
      end

      expect(routes_and_ips).to eq [
        '/help_page/1 | 126.318.035.038',
        '/contact | 184.123.665.067',
        '/home | 184.123.665.067',
        '/about/2 | 444.701.448.104'
      ]
    end
  end
end
