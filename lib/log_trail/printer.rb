# frozen_string_literal: true

class LogTrail
  # Prints the visits and unique visits on STDOUT
  class Printer
    def print(visits, unique_visits)
      puts 'Routes visit count'

      visits.each do |visit|
        puts "#{visit[0]} #{visit[1]} #{visit[1] > 1 ? 'visits' : 'visit'}"
      end

      puts "Routes unique visit count\n"

      unique_visits.each do |visit|
        puts "#{visit[0]} #{visit[1]} unique #{visit[1] > 1 ? 'views' : 'view'}"
      end
    end
  end
end
