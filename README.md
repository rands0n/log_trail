# LogTrail

Parses web server log files, extract the number of visits per page and unique ones.

## Technology

- Ruby 3.0.1
- RSpec

## Running

Clone the repository at: [https://gitlab.com/rands0n/log_trail](https://gitlab.com/rands0n/log_trail) and install the dependencies:

```sh
bundle install
```

And finally, run the parser:

```sh
./parser.rb spec/fixtures/webserver.log
```

Or, if you're more like me, you can run with docker:

```sh
docker-compose up
```

This will simply show the logs.

## Architecture Overview

The main point of the parser is `parser.rb`. It'll check if it has enough arguments on the command line, if not will trown an error. If it's okay, it'll use the `LogTrail::FileReader` to read the file. Finally, with the file in place, it will be  `analyze` by `LogTrail` class.

Thinking about separating things of the project. We have three things that needs to work together:

- Read log file line by line and extract the visit in a line;
- Store the data read by line into some kind of storage, called `Storage` on the project;
- Prints the data analyzed.

These behaviours are implemented on `LogTrail::ReadFile`, `LogTrail::Storage` and `LogTrail::Print`. All these classes are injected on the `LogTrail` class as dependencies in instance initialization.

You can view the options and helpers about how to use running `./parser.rb --help`.

## Tests

RSpec is used to write tests files and Rubocop to lint the code:

```sh
bundle exec rspec
```

```sh
bundle exec rubocop
```
