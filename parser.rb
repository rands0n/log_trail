#!/usr/bin/env ruby
# frozen_string_literal: true

require 'optparse'

require_relative './lib/log_trail'
require_relative './lib/log_trail/reader'
require_relative './lib/log_trail/storage'
require_relative './lib/log_trail/printer'

options = {}

OptionParser.new do |op|
  op.banner = 'Usage: ./parser.rb [options]'

  op.on('-f', '--file', 'WebServer file to analyze!') do |value|
    options[:file] = value
  end
end.parse!

log_file_path = ARGV[0]

if ARGV.empty?
  warn 'Error: a log file for analysis is strictly necessary.'
  exit 1
end

begin
  LogTrail.new(
    reader: LogTrail::FileReader.new(log_file_path),
    storage: LogTrail::Storage.new,
    printer: LogTrail::Printer.new
  ).analyze
rescue FileNotProvidedError
  warn 'Error: file not provided.'
  exit 1
rescue UnableToOpenFileError
  warn 'Error: unable to open file.'
  exit 1
end
