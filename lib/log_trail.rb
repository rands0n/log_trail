# frozen_string_literal: true

# The LogTrail project.
class LogTrail
  def initialize(reader:, storage:, printer:)
    @reader = reader
    @storage = storage
    @printer = printer
  end

  def analyze
    reader.each do |route, ip|
      storage.add(route, ip)
    end

    printer.print(storage.visits_count, storage.unique_visits_count)
  end

  private

  attr_reader :reader, :storage, :printer
end
