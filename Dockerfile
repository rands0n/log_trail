FROM ruby:3.0.1-alpine

ENV APP_HOME /usr/src/app
ENV BUNDLE_GEMFILE=$APP_HOME/Gemfile BUNDLE_JOBS=2 BUNDLE_PATH=/bundle

WORKDIR ${APP_HOME}

COPY Gemfile Gemfile.lock ./

RUN gem install bundler
RUN bundle install

COPY . ${APP_HOME}
