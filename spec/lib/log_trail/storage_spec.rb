# frozen_string_literal: true

require 'spec_helper'

require_relative './../../../lib/log_trail/storage'

describe LogTrail::Storage do
  let(:route_ip_pairs) do
    [
      ['/home', '451.106.204.921'],
      ['/contact', '184.123.665.067'],
      ['/about', '444.701.448.104'],
      ['/about', '444.701.448.104'],
      ['/about', '184.123.665.067']
    ]
  end

  subject { described_class.new }

  describe '#add' do
    it 'inits the count with 0 pair' do
      expect(subject.count_by('/about')).to eq 0
    end

    it 'stores route and ip pair' do
      route, ip = route_ip_pairs.first

      subject.add(route, ip)

      expect(subject.count_by(route)).to eq 1
    end

    it 'increments the count when the ip or route is the same' do
      route, ip = route_ip_pairs.first

      subject.add(route, ip)
      subject.add(route, ip)

      expect(subject.count_by(route)).to eq 2
    end
  end

  describe '#visits_count' do
    before do
      route_ip_pairs.each { |pair| subject.add(pair[0], pair[1]) }
    end

    it 'returns the count by route' do
      expect(subject.visits_count).to eq(
        [
          ['/about', 3],
          ['/home', 1],
          ['/contact', 1]
        ]
      )
    end
  end

  describe '#unique_visits_count' do
    before do
      route_ip_pairs.each { |pair| subject.add(pair[0], pair[1]) }
    end

    it 'returns unique visit count for a route' do
      expect(subject.unique_visits_count).to eq(
        [
          ['/about', 2],
          ['/home', 1],
          ['/contact', 1]
        ]
      )
    end
  end
end
