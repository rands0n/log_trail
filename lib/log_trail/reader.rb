# frozen_string_literal: true

class FileNotProvidedError < StandardError; end

class UnableToOpenFileError < StandardError; end

class LogTrail
  # Reads the file provide a method to call by each line.
  class FileReader
    def initialize(file_path)
      raise FileNotProvidedError unless File.file?(file_path)

      @log_file = read_file_from file_path
    end

    def each
      log_file.each_line do |line|
        next unless line.match?(%r{/(\w+(/\w+)?) (\d{1,3}\.){3}\d{1,3}})

        route, ip = line.split

        next unless route && ip

        yield route, ip
      end
    end

    private

    attr_reader :log_file

    def read_file_from(file_path)
      File.open(file_path, 'r')
    rescue StandardError
      raise UnableToOpenFileError
    end
  end
end
